#include "button.h"

#define _print_s(ch) Serial.print(ch)
static uint8_t pin_data[4];
void button_init(int *bt_data, uint8_t size){
    for(uint8_t i = 0; i < size; i++){
        pinMode(bt_data[i], INPUT_PULLUP);
        //digitalWrite(bt_data[i], HIGH);
        pin_data[i] = bt_data[i];
    }
    
}

#define bt_read(pin) digitalRead(pin)
boolean bt1_check(){
    int pin = pin_data[0];
    static uint32_t _bt_time;
    static uint8_t _bt_status;
    boolean _check = false;
    switch (_bt_status)
    {
    case 0:
        if(!bt_read(pin)){
            _bt_status = 1;
            _bt_time = millis();
        }
        break;
    case 1:
        if(millis() - _bt_time > 20){
            if(!bt_read(pin)){
                _bt_status = 2;
            }
            _bt_time = millis();
        }
        break;
    case 2:
        if(bt_read(pin)){
            _check = true;
            _bt_time = 0;
            _bt_status = 0;
        }        
    default:
        break;
    }

    return _check;
}

boolean bt2_check(){
    int pin = pin_data[1];
    static uint32_t _bt_time;
    static uint8_t _bt_status;
    boolean _check = false;
    switch (_bt_status)
    {
    case 0:
        if(!bt_read(pin)){
            _bt_status = 1;
            _bt_time = millis();
        }
        break;
    case 1:
        if(millis() - _bt_time > 20){
            if(!bt_read(pin)){
                _bt_status = 2;
            }
            _bt_time = millis();
        }
        break;
    case 2:
        if(bt_read(pin)){
            _check = true;
            _bt_time = 0;
            _bt_status = 0;
        }        
    default:
        break;
    }

    return _check;
}

boolean bt3_check(){
    int pin = pin_data[2];
    static uint32_t _bt_time;
    static uint8_t _bt_status;
    boolean _check = false;
    switch (_bt_status)
    {
    case 0:
        if(!bt_read(pin)){
            _bt_status = 1;
            _bt_time = millis();
        }
        break;
    case 1:
        if(millis() - _bt_time > 20){
            if(!bt_read(pin)){
                _bt_status = 2;
            }
            _bt_time = millis();
        }
        break;
    case 2:
        if(bt_read(pin)){
            _check = true;
            _bt_time = 0;
            _bt_status = 0;
        }        
    default:
        break;
    }

    return _check;
}

boolean bt4_check(){
    int pin = pin_data[3];
    static uint32_t _bt_time;
    static uint8_t _bt_status;
    boolean _check = false;
    switch (_bt_status)
    {
    case 0:
        if(!bt_read(pin)){
            _bt_status = 1;
            _bt_time = millis();
        }
        break;
    case 1:
        if(millis() - _bt_time > 20){
            if(!bt_read(pin)){
                _bt_status = 2;
            }
            _bt_time = millis();
        }
        break;
    case 2:
        if(bt_read(pin)){
            _check = true;
            _bt_time = 0;
            _bt_status = 0;
        }        
    default:
        break;
    }

    return _check;
}