/*
    LM35



    Tran Vo Phuoc Dat
    date: 09/05/2019
*/
#ifndef __LM35_H
#define __LM35_H

#include "Arduino.h"

#ifdef __cplusplus
extern "C" {
#endif

float read_temp(int pin_LM35);


#ifdef __cplusplus
}
#endif

#endif //__LM35_