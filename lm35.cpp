#include "lm35.h"
#include "conf.h"
float read_temp(int pin_LM35){
  static float temp;
  static uint32_t _lm35_time;
  if(millis() - _lm35_time > 1000){
    temp = analogRead(pin_LM35)*100.0*5.0/1024.0;
    _print_s("temp: "); _print_s(temp); _print_s("\n");
    _lm35_time = millis();
  }
  return temp;
}