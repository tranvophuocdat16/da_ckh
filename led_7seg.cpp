#include "led_7seg.h"


static int _HC595_CLK;
static int _HC595_DATA;
static int _HC595_LOAD;

const uint8_t _7seg_hex[] = {
    B11000000, //0
    B11111001, //1 
    B10100100, //2
    B10110000, //3 
    B10011001, //4
    B10010010, //5
    B10000011, //6
    B11111000, //7
    B10000000, //8
    B10011000 //9
};

void led_2_7seg_init(int pin_data, int pin_clk, int pin_load){
    _HC595_LOAD = pin_load;
    _HC595_DATA = pin_data;
    _HC595_CLK = pin_clk;
    pinMode(_HC595_LOAD, OUTPUT);
    pinMode(_HC595_DATA, OUTPUT);
    pinMode(_HC595_CLK, OUTPUT);
}

#define HC595_COUNT 2
void shiftOutHC595(uint8_t *data) {
  digitalWrite(_HC595_LOAD, LOW);
  for (int i = 0; i < HC595_COUNT; i++) {
    shiftOut(_HC595_DATA, _HC595_CLK,1,data[i]);
  }
  digitalWrite(_HC595_LOAD, HIGH);
}

void led_2_7seg_print(uint8_t value){
    uint8_t digit[HC595_COUNT];
    digit[0] = _7seg_hex[value % 10];
    digit[1] = _7seg_hex[(value / 10) % 10];
    //Send them to 7 segment displays
    shiftOutHC595(digit);
}

