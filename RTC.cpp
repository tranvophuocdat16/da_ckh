#include "RTC.h"
void RTC_begin(){ 
    Wire.begin();
}
uint8_t bcd2dec(uint8_t num){
        return ((num/16 * 10) + (num % 16));
}
uint8_t dec2bcd(uint8_t num){
        return ((num/10 * 16) + (num % 10));
}
void RTC_SetTime(RTC_DATA rtc_data){
    Wire.beginTransmission(DS1307_ADDR);
    Wire.write((uint8_t)0x00);
    Wire.write(dec2bcd(rtc_data.second));
    Wire.write(dec2bcd(rtc_data.minute));
    Wire.write(dec2bcd(rtc_data.hour));
    Wire.write(dec2bcd(rtc_data.wday)); // day of week: Sunday = 1, Saturday = 7
    Wire.write(dec2bcd(rtc_data.day)); 
    Wire.write(dec2bcd(rtc_data.month));
    Wire.write(dec2bcd(rtc_data.year));
    Wire.endTransmission();
}
RTC_DATA RTC_ReadTime(){
    RTC_DATA _rtc_data;
    Wire.beginTransmission(DS1307_ADDR);
    Wire.write((uint8_t)0x00);
    Wire.endTransmission();
    Wire.requestFrom(DS1307_ADDR, NumberOfFields);
    _rtc_data.second = bcd2dec(Wire.read() & 0x7f);
    _rtc_data.minute = bcd2dec(Wire.read() );
    _rtc_data.hour   = bcd2dec(Wire.read() & 0x3f); // chế độ 24h.
    _rtc_data.wday   = bcd2dec(Wire.read() );
    _rtc_data.day    = bcd2dec(Wire.read() );
    _rtc_data.month  = bcd2dec(Wire.read() );
    _rtc_data.year   = bcd2dec(Wire.read() );
    return _rtc_data;
}
void RTC_PrintSerial(RTC_DATA rtc_data){
    static uint8_t _second;
    if (_second != rtc_data.second){
        Serial.print("RTC time: ");
        Serial.print(rtc_data.hour);
        Serial.print(":");
        Serial.print(rtc_data.minute);
        Serial.print(":");
        Serial.print(rtc_data.second);
        Serial.print(" ");
        Serial.print(rtc_data.day);
        Serial.print(" ");
        Serial.print(rtc_data.month);
        Serial.print(" ");
        Serial.print(rtc_data.year+2000); 
        Serial.println();
        _second = rtc_data.second;
    }
}
void Alarm_set_time(ALARM_DATA alarm_data){
    EEPROM.write(Alarm_eeprom + 0, alarm_data.second);
    EEPROM.write(Alarm_eeprom + 1, alarm_data.minute);
    EEPROM.write(Alarm_eeprom + 2, alarm_data.hour);
    EEPROM.write(Alarm_eeprom + 3, alarm_data.day);
    EEPROM.write(Alarm_eeprom + 4, alarm_data.month);
    EEPROM.write(EEPROM_AL_TIME_ADDR, 1);
}
void Alarm_set_countdown(RTC_DATA rtc_data, uint32_t second){
    EEPROM.write(Alarm_eeprom + 5,  (uint8_t)(second>>24));
    EEPROM.write(Alarm_eeprom + 6,  (uint8_t)(second>>16));
    EEPROM.write(Alarm_eeprom + 7,  (uint8_t)(second>>8));
    EEPROM.write(Alarm_eeprom + 8,  (uint8_t)(second));
    EEPROM.write(Alarm_eeprom + 9,  rtc_data.second);
    EEPROM.write(Alarm_eeprom + 10, rtc_data.minute);
    EEPROM.write(Alarm_eeprom + 11, rtc_data.hour);
    EEPROM.write(Alarm_eeprom + 12, rtc_data.day);
    EEPROM.write(EEPROM_AL_CD_ADDR, 1);
}
ALARM_DATA Alarm_read_time(){
    ALARM_DATA alarm_data;
    alarm_data.second   = EEPROM.read(Alarm_eeprom + 0);
    alarm_data.minute   = EEPROM.read(Alarm_eeprom + 1);
    alarm_data.hour     = EEPROM.read(Alarm_eeprom + 2);
    alarm_data.day      = EEPROM.read(Alarm_eeprom + 3);
    alarm_data.month    = EEPROM.read(Alarm_eeprom + 4);
    //Serial.print("Alarm Time: ");
    //Serial.print(alarm_data.hour);
    //Serial.print(":");
    //Serial.print(alarm_data.minute);
    //Serial.print(":");
    //Serial.print(alarm_data.second);
    //Serial.print(" ");
    //Serial.print(alarm_data.day);
    //Serial.print(" ");
    //Serial.println(alarm_data.month);
    return alarm_data;
}
uint32_t Alarm_read_countdown(RTC_DATA *rtc_data_coutdown){
    uint32_t eeprom_data_4_addr;
    for(uint8_t i = 0; i < 4; i++){
        eeprom_data_4_addr = (eeprom_data_4_addr<<8) | ((uint8_t) EEPROM.read(Alarm_eeprom + 5+ i));
    }
    rtc_data_coutdown->second   = EEPROM.read(Alarm_eeprom + 9);
    rtc_data_coutdown->minute   = EEPROM.read(Alarm_eeprom + 10);
    rtc_data_coutdown->hour     = EEPROM.read(Alarm_eeprom + 11);
    rtc_data_coutdown->day      = EEPROM.read(Alarm_eeprom + 12);
    //Serial.print("countdown: "); Serial.println(eeprom_data_4_addr);
    return eeprom_data_4_addr;
}
uint32_t Al_read_sec_set(){
    uint32_t eeprom_data_4_addr;
    for(uint8_t i = 0; i < 4; i++){
        eeprom_data_4_addr = (eeprom_data_4_addr<<8) | ((uint8_t) EEPROM.read(Alarm_eeprom + 5+ i));
    }
    return eeprom_data_4_addr;
}
boolean Alarm_Time(RTC_DATA rtc_data, uint8_t _delay){
    _delay--;
    boolean check = false;
    ALARM_DATA alarm_data = Alarm_read_time();
    uint16_t RTC_second_q = rtc_data.second + rtc_data.minute*60 + rtc_data.hour*3600;
    uint16_t ALARM_second_q = alarm_data.second + alarm_data.minute*60 + alarm_data.hour*3600;
    //Serial.print("RTC s: "); Serial.print(RTC_second_q);
    //Serial.print(" ALARM s: "); Serial.println(ALARM_second_q);
    if(EEPROM.read(EEPROM_AL_TIME_ADDR) == 1){
        if((rtc_data.month == alarm_data.month) && (rtc_data.day == alarm_data.day)){
            if((RTC_second_q >= ALARM_second_q) && (RTC_second_q <= ALARM_second_q + _delay)){
                check = true;
            }
            else if(RTC_second_q >= ALARM_second_q + _delay){
                EEPROM.write(EEPROM_AL_TIME_ADDR, 0);
            }
        }
    }
    return check;
}
static uint32_t _al_count;
boolean Alarm_countdown(RTC_DATA rtc_data, uint8_t _delay){
    _delay--;
    RTC_DATA rtc_data_las;
    boolean check = false;
    uint16_t second = Alarm_read_countdown(&rtc_data_las);
    uint16_t RTC_second_q = rtc_data.second + rtc_data.minute*60 + rtc_data.hour*3600;
    static uint16_t __time;
    if(rtc_data.day == rtc_data_las.day){
        __time = RTC_second_q + 1;
    }
    else{
        RTC_second_q += __time;
    }
    uint32_t RTC_second_las_q = rtc_data_las.second + rtc_data_las.minute*60 + rtc_data_las.hour*3600;
    //Serial.print("RTC s: "); Serial.print(RTC_second_q);
    //Serial.print(" ALARM s: "); Serial.println(RTC_second_las_q);
    if(EEPROM.read(EEPROM_AL_CD_ADDR) == 1)
    {
        //Serial.print("TIME COUNTDOWN"); Serial.println(RTC_second_q - RTC_second_las_q);
        if((RTC_second_q - RTC_second_las_q >= second) && (RTC_second_q - RTC_second_las_q <= second + _delay)){
            check = true;
        }
        else if(RTC_second_q - RTC_second_las_q >= second + _delay){
            EEPROM.write(EEPROM_AL_CD_ADDR, 0);
        }
        _al_count = RTC_second_q - RTC_second_las_q;
    }
    return check;
}

uint32_t read_countdow(){
    return _al_count;
}

uint8_t read_check_al(){
    return EEPROM.read(EEPROM_AL_CD_ADDR);
}
void clear_check_al(){
    EEPROM.write(EEPROM_AL_CD_ADDR, 0);
}