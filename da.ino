#include "RTC.h"
#include "led_7seg.h"
#include "button.h"
#include "lm35.h"
#include "manual.h"
#include "lcd_interf.h"
//
int buzzer = 15;
int res_temp = 16;
//
float read_t;
//
RTC_DATA rtc_data;
RTC_DATA al_data;
RTC_DATA rtc_count;
int button_data[4];
float temp_read;
uint8_t _s_lcd_print[16];
//
LiquidCrystal lcd(6, 5, 4, 3, 2, 14);

typedef enum{
  MODE_AUTO =0X0UL,
  MODE_MANUAL = 0x1UL,
}MODE;

static MODE mode;

void setup() {
  Serial.begin(115200);
  //RTC
  RTC_begin();
  mode = MODE_AUTO;
  pinMode(buzzer, OUTPUT);
  digitalWrite(buzzer, LOW);
  pinMode(buzzer, OUTPUT);
  digitalWrite(buzzer, LOW);
  /*
  rtc_data.second = 0;
  rtc_data.minute = 53;
  rtc_data.hour = 21;
  rtc_data.wday = 4;
  rtc_data.day =  9;
  rtc_data.month = 5;
  rtc_data.year = 19;
  RTC_SetTime(rtc_data);
  */
  led_2_7seg_init(7,8,9);
  //button
  button_data[0] = 10;
  button_data[1] = 11;
  button_data[2] = 12;
  button_data[3] = 13;
  button_init(button_data, 4);
  //
  lcd_init(&lcd);
  clear_check_al();
  //Alarm_set_countdown(RTC_ReadTime(), 30);
}

void loop() {
  if(!read_check_al()){
    digitalWrite(res_temp, LOW);
    rtc_data = RTC_ReadTime();
    RTC_PrintSerial(rtc_data);
    //manual_handle();
    //lcd_print_time(rtc_data);
    //lcd_print_temp(read_temp(A0));
    lcd_print_t_t(rtc_data, read_temp(A0));
    
    if(bt1_check()){
      
      if(mode == MODE_AUTO){
        mode = MODE_MANUAL;
      }
      else if(mode == MODE_MANUAL){
        mode = MODE_AUTO; 
      }
      _print_s("mode : "); _print_s(mode); _print_s("\n");
      _print_s("bt 1\n");
    }
    lcd.setCursor(0,1);
    if(mode == MODE_AUTO){
      lcd.print("MODE: AUTO      ");
      if(bt4_check()){
        al_data.second = 0;
        al_data.minute = 0;
        al_data.hour = 5;
        Alarm_set_countdown(RTC_ReadTime(), 28800);
        lcd.clear();
        lcd.setCursor(0,0);
        lcd.print("set auto 8 hour");
        delay(1000);
        lcd.clear();
      }
    }
    if(mode == MODE_MANUAL){
      lcd.print("MODE: MANUAL    ");
      if(bt4_check()){
        lcd.clear();
        uint8_t _s_lcd[16];
        uint16_t value = 50;
        uint8_t _status = 0;
        uint32_t _s_h;
        while (true){
          if(_status == 0){
            lcd.setCursor(0,0);
            lcd.print("set time :");
            lcd.setCursor(5,1);
            sprintf(_s_lcd, "%d.%d hour", value/10, value%10);
            lcd.print((const char *)_s_lcd);
            value_handle(&value, 5);
            if(bt4_check()){
              _s_h = (value/10)*3600 + (value%10*1800);
              Alarm_set_countdown(RTC_ReadTime(), _s_h);
              lcd.clear();
              _status = 1;
              lcd.setCursor(7,1);
              lcd.print("OK");
              delay(1000);
            }
            if(bt1_check()){
              _status = 1;
            }
          }
          else if(_status == 1){
            lcd.clear();
            break;
          }
        }
      }
    }
  }
  while (read_check_al())
  {
    lcd.setCursor(0,0);
    lcd.print("time :");
    rtc_data = RTC_ReadTime();
    lcd.setCursor(0,1);
    rtc_count = conv_sec_to_rtc(Al_read_sec_set() - read_countdow());
    res_temp_handle();
    lcd_print_count_time(rtc_count, read_temp(A0));
    lcd.print((const char*)_s_lcd_print);
    if(Alarm_countdown(rtc_data, 5)){
      uint32_t _cls_buzz = millis();
      lcd.clear();
      while (millis() - _cls_buzz < 5000)
      {
        lcd.setCursor(6,0);
        lcd.print("DONE");
        if(millis() - _cls_buzz < 500){
          digitalWrite(buzzer, HIGH);
        }
        else if(millis() - _cls_buzz < 1500){
          digitalWrite(buzzer, LOW);
        }
        else if(millis() - _cls_buzz < 2000){
          digitalWrite(buzzer, LOW);
        }
        else if(millis() - _cls_buzz < 3500){
          digitalWrite(buzzer, HIGH);
        }
        else {
          digitalWrite(buzzer, LOW);
        }
      }
      lcd.clear();
      break;
    }
    if(bt1_check()){
      lcd.clear();
      lcd.setCursor(0,0);
      lcd.print("exit ?");
      uint32_t _time_out = millis();
      while (true){
        if(millis() - _time_out > 5000){
          break;
        }
        if(bt1_check()){
          lcd.setCursor(0,0);
          lcd.print("exit ?  NO");
          delay(1000);
          lcd.clear();
          break;
        }
        if(bt4_check()){
          clear_check_al();
          lcd.setCursor(0,0);
          lcd.print("exit ?  OK");
          delay(1000);
          lcd.clear();
          break;
        }
      }
    }
  }
}

void value_handle(uint16_t *value, uint8_t step){
  if(bt2_check()){
      *value += step;
      _print_s("tang\n");
  }
  if(bt3_check()){
      *value -= step;
      _print_s("giam\n");
  }
}

RTC_DATA conv_sec_to_rtc(uint32_t sec){
  RTC_DATA _data;
  _data.hour = sec/3600;
  _data.minute = (sec - _data.hour*3600)/60;
  _data.second = (sec - _data.hour*3600 - _data.second);
  return _data;
}

#define _ol_temp 45
void res_temp_handle(){
  float _t = read_temp(A0);
  if(_t > _ol_temp){
    digitalWrite(res_temp, LOW);
  }
  else{
    digitalWrite(res_temp, HIGH);
  }
  
}