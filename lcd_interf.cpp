#include "lcd_interf.h"
static uint8_t _data_print[16];
static LiquidCrystal *_lcd;
void lcd_init(LiquidCrystal *lcd){
    _lcd = lcd;
    _lcd->begin(16, 2);

}

void lcd_print_time(RTC_DATA data){
    sprintf(_data_print, "time: %d:%d:%d", data.hour, data.minute, data.second);
    _lcd->setCursor(0, 0);
    _lcd->print((const char*)_data_print);
}
void lcd_print_temp(float temp){
    uint8_t t = (uint8_t) temp;
    sprintf(_data_print, "temp: %d*C     ", t);
    _lcd->setCursor(0, 1);
    _lcd->print((const char*)_data_print);
}
void lcd_print_t_t(RTC_DATA data, float temp){
    uint8_t _temporary;
    memset(_data_print, ' ', 16);
    _temporary = data.hour;
    _data_print[0] = _temporary/10%10 + 48;
    _data_print[1] = _temporary%10 + 48;
    _data_print[2] = ':';
    _temporary = data.minute;
    _data_print[3] = _temporary/10%10 + 48;
    _data_print[4] = _temporary%10 + 48;
    _data_print[5] = ':';
    _temporary = data.second;
    _data_print[6] = _temporary/10%10 + 48;
    _data_print[7] = _temporary%10 + 48;
    //
    /*
    _temporary = temp;
    _data_print[10] = _temporary/10%10 + 48;
    _data_print[11] = _temporary%10 + 48;
    _data_print[12] = '.';
    _data_print[13] = _temporary*10%10 + 48;
    _data_print[14] = '*';
    _data_print[15] = 'c';
    */
    _lcd->setCursor(0, 0);
    _lcd->print((const char*)_data_print);
}
void lcd_print_count_time(RTC_DATA data, float temp){
    uint8_t _temporary;
    uint8_t _data_print_1[11];
    memset(_data_print_1, ' ', 16);
    _temporary = data.hour;
    _lcd->setCursor(0, 0);
    _lcd->print("time: ");
    _data_print_1[0] = _temporary/10%10 + 48;
    _data_print_1[1] = _temporary%10 + 48;
    _data_print_1[2] = ':';
    _temporary = data.minute;
    _data_print_1[3] = _temporary/10%10 + 48;
    _data_print_1[4] = _temporary%10 + 48;
    _data_print_1[5] = ':';
    _temporary = data.second;
    _data_print_1[6] = _temporary/10%10 + 48;
    _data_print_1[7] = _temporary%10 + 48;
    _lcd->print((const char*)_data_print_1);
    //
    _lcd->setCursor(0, 1);
    _lcd->print("temp: ");
    memset(_data_print_1, ' ', 16);
    _temporary = temp;
    _data_print_1[0] = _temporary/10%10 + 48;
    _data_print_1[1] = _temporary%10 + 48;
    _data_print_1[2] = '.';
    _data_print_1[3] = _temporary*10%10 + 48;
    _data_print_1[4] = '*';
    _data_print_1[5] = 'c';
    _lcd->print((const char*)_data_print_1);
}