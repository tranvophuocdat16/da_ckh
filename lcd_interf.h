/*
    lcd



    Tran Vo Phuoc Dat
    date: 09/05/2019
*/
#ifndef __LCD_INTERF_H
#define __LCD_INTERF_H

#include "LiquidCrystal.h"
#include "conf.h"

// 

#ifdef __cplusplus
extern "C" {
#endif

void lcd_init(LiquidCrystal *lcd);
void lcd_print_time(RTC_DATA data);
void lcd_print_temp(float temp);
void lcd_print_t_t(RTC_DATA data, float temp);
void lcd_print_count_time(RTC_DATA data, float temp);

#ifdef __cplusplus
}
#endif

#endif //__LCD_INTERF_H