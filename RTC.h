/*
    RTC



    Tran Vo Phuoc Dat
    date: 1/1/2019
*/
#ifndef __RTC_H
#define __RTC_h

#include <Wire.h>
#include <EEPROM.h>
#include <Arduino.h>
#include "conf.h"
#define DS1307_ADDR 0x68
#define NumberOfFields 7

#define Alarm_eeprom 0

#define EEPROM_AL_TIME_ADDR 255
#define EEPROM_AL_CD_ADDR 254

 
#ifdef __cplusplus
extern "C" {
#endif

void RTC_begin();
uint8_t bcd2dec(uint8_t num);
uint8_t dec2bcd(uint8_t num);
void RTC_SetTime(RTC_DATA rtc_data);
RTC_DATA RTC_ReadTime();
void RTC_PrintSerial(RTC_DATA rtc_data);
void Alarm_set_time(ALARM_DATA alarm_data);
void Alarm_set_countdown(RTC_DATA rtc_data, uint32_t second);
ALARM_DATA Alarm_read_time();
uint32_t Alarm_read_countdown(RTC_DATA *rtc_data_coutdown);
boolean Alarm_Time(RTC_DATA rtc_data, uint8_t _delay);
boolean Alarm_countdown(RTC_DATA rtc_data, uint8_t _delay);
uint8_t read_check_al();
void clear_check_al();
uint32_t read_countdow();
uint32_t Al_read_sec_set();

#ifdef __cplusplus
}
#endif
#endif