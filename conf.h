#ifndef __CONF_H
#define __CONF_H

#include <Arduino.h>
#define _print_s(ch) Serial.print(ch)

#ifdef __cplusplus
extern "C"{
#endif
//***********E30***********
#define E30_ADDR_SLAVE 1457
#define E30_ADDR_HUB 10
#define E30_M 13
#define E30_UART Serial2
//*************************
//***********MFD***********
//thoi gian cho may no
#define TIME_WAIT_START 5000
//thoi gian cho de do dien ap dau ra
#define TIME_WAIT_READ_220V 5000
//so lan khoi dong lai may phat
#define COUNT_ERR 3
//
#define choke_out1 6
#define choke_out2 7
#define pin_key_on_off 8
#define pin_key_start 9
#define pin_ATS 10
#define pin_read_vol_220 3
//*************************
//**********PZE************
#define PZE_UART Serial1
//*************************

typedef struct
{
    uint8_t second;
    uint8_t minute;
    uint8_t hour;
    uint8_t day;
    uint8_t wday;
    uint8_t month;
    uint8_t year;
} RTC_DATA;
typedef struct
{
    uint8_t second;
    uint8_t minute;
    uint8_t hour;
    uint8_t day;
    uint8_t month;
} ALARM_DATA;
/*
typedef enum
{
    MFD_OFF = 0x00U,
    MFD_ON = 0x01U,
    MFD_FAIL = 0X02U
} MFD_STATE_CMD;
typedef enum
{
    MFD_sate_start = 0x0U,
    MFD_sate_on = 0x1U,
    MFD_sate_off = 0x2U,
    MFD_sate_fail = 0x3U,
    MFD_sate_wait = 0x4U
} MFD_sate;
typedef struct
{
    uint8_t data[255];
    uint8_t size;
    } PZE_DATA_UART;
    typedef struct
    {
        float volt;
        float amp;
    } PZE_DATA;
    typedef struct
    {
        MFD_sate mfd_state;
        PZE_DATA pze_data;
    } MFD_DATA_FEEDBACK;
    */
#ifdef __cplusplus
}
#endif


#endif //__CONF_H