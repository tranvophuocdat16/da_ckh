/*
    LED 7seg 74hc595



    Tran Vo Phuoc Dat
    date: 09/05/2019
*/
#ifndef __LED_7SEG_H
#define __LED_7SEG_H

#include <Arduino.h>
/*
#include <ShiftRegister74HC595.h>
#define _HC595_2_7SEG_PIN_DATA 8
#define _HC595_2_7SEG_PIN_CLK 9
#define _HC595_2_7SEG_PIN_LOAD 10
ShiftRegister74HC595 sr (2, _HC595_2_7SEG_PIN_DATA, _HC595_2_7SEG_PIN_CLK, _HC595_2_7SEG_PIN_LOAD);
*/

#ifdef __cplusplus
extern "C" {
#endif

void led_2_7seg_init(int pin_data, int pin_clk, int pin_load);
void led_2_7seg_print(uint8_t value);



#ifdef __cplusplus
}
#endif

#endif // __LED_7SEG_H