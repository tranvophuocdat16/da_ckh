/*
    button



    Tran Vo Phuoc Dat
    date: 09/05/2019
*/
#ifndef __BUTTON_H
#define __BUTTON_H

#include "Arduino.h"

#ifdef __cplusplus
extern "C" {
#endif


void button_init(int *bt_data, uint8_t size);

boolean bt1_check();
boolean bt2_check();
boolean bt3_check();
boolean bt4_check();

#ifdef __cplusplus
}
#endif

#endif //__BUTTON_H